## WEB 3D : Conquérir internet en 3 dimensions

Ceci est le repo pour le talk WEB 3D: Conquérir internet en 3 dimension.
Les slides sont disponibles [ici](https://bit.ly/semleti-web3d-slides)

Ou dans le repo même, dans les fichiers .pptx ou .pdf

## UTILISATION

Vous êtes libres d'utiliser ce talk.

Pour le lancer:

`git clone https://gitlab.com/seiler.r/talk.git`

`cd talk`

`npm i`

`npm start`

## SOUS-TITRES

Les sous-titres sont disponibles [ici](bit.ly/semleti-web3d-sub)

La synchronisation des sous-titres n'est pas encore accessible au public.

Pour synchroniser les sous-titres, il est nécessaire d'utiliser l'extension dans le dossier extensions\chrome\slides-subtitles-coordinator

