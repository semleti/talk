let dependencies = [
  "https://www.gstatic.com/firebasejs/7.2.1/firebase-app.js",
  "https://www.gstatic.com/firebasejs/7.2.1/firebase-auth.js",
  "https://www.gstatic.com/firebasejs/7.2.1/firebase-firestore.js"
];

function loadDeps(index) {
  if (index < dependencies.length) {
    console.log(index, dependencies[index]);
    let s = document.createElement("script");
    s.src = dependencies[index];
    s.onload = _ => loadDeps(index + 1);
    document.head.appendChild(s);
  } else {
    loaded();
  }
}

loadDeps(0);

function loaded() {
  let s = document.createElement("script");
  s.innerHTML = c.toString() + "c()";
  document.head.appendChild(s);
}

function c() {
  console.log(window);
  console.log(window.firebase);
  console.log(firebase);
  var firebaseConfig = {
    apiKey: "AIzaSyAFYR0CGNXe2X4fJDRd2-y4O0zqISS2mjY",
    authDomain: "slidetitle.firebaseapp.com",
    databaseURL: "https://slidetitle.firebaseio.com",
    projectId: "slidetitle",
    storageBucket: "slidetitle.appspot.com",
    messagingSenderId: "sender-id",
    appId: "slidetitle"
  };

  // Initialize Firebase
  window.firebase.initializeApp(firebaseConfig);

  var db = firebase.firestore();

  var provider = new firebase.auth.GoogleAuthProvider();

  var docRef;

  firebase.auth().onAuthStateChanged(function(user) {
    if (user) {
      console.log(user);
      // User is signed in.
      var displayName = user.displayName;
      var email = user.email;
      var emailVerified = user.emailVerified;
      var photoURL = user.photoURL;
      var isAnonymous = user.isAnonymous;
      var uid = user.uid;
      var providerData = user.providerData;

      docRef = db.collection(`users/${uid}/presentations`).doc('Rf82pgVFTJr2aCYF6GIn');
      // ...
    } else {
      console.log("logged out");
      // User is signed out.
      // ...
      firebase
        .auth()
        .signInWithEmailAndPassword(
          prompt("email"),
          prompt("password") ||
            "#6eKLOmh%xd2$lNTnZT42FMGxZb^%3fXACHeFe9XSgKLL0pASz0zWKlz3P*7*PTw*UF7@h&Vyr6YEc3TfuSum9c1mT@$4InyAzs"
        )
        .catch(function(error) {
          console.log(error);
          // Handle Errors here.
          var errorCode = error.code;
          var errorMessage = error.message;
          // ...
        })
    }
  });

  document
    .querySelector("#punch-start-presentation-left")
    .addEventListener("click", e =>
      setTimeout(_ => {
        let diaElt = document
          .querySelector("iframe.punch-present-iframe")
          .contentDocument.querySelector(
            "div[role=listbox] > div[role=option]"
          );
        var observer = new MutationObserver(function(mutations) {
          let slideNumber = parseInt(diaElt.getAttribute("aria-posinset"));
          if(docRef) {
            docRef.set({slideNumber:slideNumber},{merge:true})
          }
        });
        observer.observe(diaElt, {
          attributes: true,
          attributeFilter: ["aria-posinset"]
        });
        let slideNumber = parseInt(diaElt.getAttribute("aria-posinset"));
        if(docRef) {
          docRef.set({slideNumber:slideNumber},{merge:true})
        }
      }, 1000)
    );
}
