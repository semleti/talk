let dependencies = [{ type: "module", src: "https://google.com/model-viewer" }];
let modifications = [
  { selector: "#myId", type: "replace_inner", value: "<p>hello</p>" }
];
let style = `
p {
	color: red;
}
`;

let dependencyElements = [];

function init() {
  createDependencies();

  let styleElement = document.createElement("style");
  document.head.appendChild(styleElement);

  applyStyle();

  applyModifications();
}

init();

function createDependencies() {
  dependencies.forEach(dep => {
    createDependency(dep);
  });
}

function removeDependencies() {
  dependencyElements.forEach(dep => {
    dep.parentElement.removeChld(dep);
  });
  dependencyElements = [];
}

function createDependency(dep) {
  let e = document.createElement("script");
  e.type = dep.type;
  e.src = e.src;
  document.head.appendChild(e);
  dependencyElements.push(e);
}

function applyDependencies() {
  //remove all the old dependencies
  removeDependencies();

  //add the new dependencies
  createDependencies();
}

function applyStyle() {
  styleElement.innerHTML = style;
}

function applyModifications() {
  modifications.forEach(mod => {
    document.querySelectorAll(mod.selector).forEach(e => {
      if (mod.type === "replace_inner") {
        e.innerHTML = mod.value;
      }
    });
  });
}

function applyAll() {
  //apply the dependencies
  applyDependencies();

  //set the new style
  applyStyle();

  //apply the modifications
  applyModifications();
}
