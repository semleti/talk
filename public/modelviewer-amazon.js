let script = document.createElement("script");
// script.src = "https://unpkg.com/@google/model-viewer/dist/model-viewer-legacy.js";
script.src = "https://localhost:8080/model-viewer-legacy.js";
document.head.appendChild(script);

let img = document.querySelector("#altImages > ul > li:nth-child(4) img");
img.src = "https://localhost:8080/3d.png";

let content = document.querySelector("#imgTagWrapperId");
content.innerHTML = `<model-viewer src="https://localhost:8080/laptop.glb" alt="A 3D model of an astronaut"
   auto-rotate="" camera-controls="" background-color="#455A64"></model-viewer>`;

let large_content = document.querySelector("#ivStage");
large_content.innerHTML = `<model-viewer src="https://localhost:8080/laptop.glb" alt="A 3D model of an astronaut"
   auto-rotate="" camera-controls="" background-color="#455A64"></model-viewer>`;

let style = document.createElement("style");
style.innerHTML = `#imageBlock #main-image-container .a-declarative {display: block !important;}
   #imgTagWrapperId {display: block !important;}
   #imgTagWrapperId > model-viewer {width: 100% !important; height: 100% !important;}
   #ivStage > model-viewer {width: 100% !important; height: 100% !important;}
   #ivImage_0 > .ivThumbImage {
       background: url("https://localhost:8080/3d.png") center center no-repeat !important
    }`;
document.head.appendChild(style);
